<?php
/**
  * *****    GENERADOR DE IMAGEN CAPTCHA CON PHP     *****
  *
  * Para evitar los robots de internet utiliza una fuente
  * que genera una letra dentro de un naipe de Poker evitando
  * que el robot pueda reconocer el caracter.
  * 
  * Modo de uso:
  * Se debe colocar la ruta a captcha.php como si fuera
  * un archivo de imagen. La fuente debe estar en la misma 
  * ruta que el script
  *
  * <img src="img/captcha.php">
  *
  * app/
  *  └ img/
  *      └ captcha.php 
  *      └ font.ttf
  *
  * Para poder utilizarlo en formularios, el script almacena 
  * los caracteres en la variable global $_SESSION['captcha']
  * para poder validar con el imput recibido del formulario
  * 
  * Importante, hay que tener instalada la libreria GD de php
  * 
  * @version 1.0
  * @author Prof. Andrés D. Romano
  */
session_start();
header("Content-Type: image/png");
# Crear el objeto de la imagen
# Parametros (width, height) para crear la imagen que es un cuadro blanco
$width = 300;
$height = 80;
$img = imagecreatetruecolor($width, $height);
# Crear colores
$blanco = imagecolorallocate($img, 255, 255, 255);
# Usar el color para llenar el rectangulo
# Parametros (image, x1, y1, x2, y2, color)
imagefilledrectangle($img, 0, 0, 300, 80, $blanco);
# Crear una cadena aleatoria
$min = 4;
$max = 7;
$largo = rand($min,$max); // para que no sea siempre el mismo tamaño
$caracteres = "abcdefghijklmnopqrstuvwxyz"; // la fuente elegida solo acepta minusculas
$texto = "";
$limite = strlen($caracteres);
for ($i=0; $i < $largo; $i++){
    $texto .= $caracteres[rand(0, $limite)];//el .= va a ir concatenando los caracteres
}
# Archivo de la fuente
$fuente = "./font.ttf";
# Parametros (image, size, angle, x, y, color, fontfile, text)
imagettftext($img, 40, 0, 15, 50, $gris, $fuente, $texto);
# Asignamos el texto al array SESSION
$_SESSION['captcha'] = $texto;
# desplegar el rectángulo
imagepng($img);
# destruir la imagen para vaciar la memoria
imagedestroy($img);
