<?php 
/**
 * Este script utiliza la libreria PHPExcel para leer 
 * un archivo .xlsx, es un ejemplo basico con el fin 
 * de poder utilizar eso datos del archivo para algun
 * otro proposito, se debe descomprimir el archivo
 * PHPExcel.zip que contiene la libreria
 * 
 */
# incluyo la libreria
require_once "PHPExcel/Classes/PHPExcel/IOFactory.php";

# especifico la ruta del archivo
$fileExcel = "clientes.xlsx";

# abrir el archivo
$dataExcel = PHPExcel_IOFactory::load($fileExcel);

# elijir la hoja
$dataExcel->setActiveSheetIndex(0);

# saber cuantas filas y columnas tiene el archivo
#$filas = $dataExcel->setActiveSheetIndex(0)->getHighestRow();
#$colum = $dataExcel->setActiveSheetIndex(0)->getHighestColumn();

#echo "Cantidad de filas ".$filas;
#echo "Cantidad de colum ".$colum;

$dni = '';
# leer el valor de una celda
for ($i=2; $i < $filas; $i++) { 

	if($i % 2 == 0) {
		# obtengo el dato de a celda C
		$dni = $dataExcel->setActiveSheetIndex()->getCell('C'.$i)->getCalculatedValue();
		echo $dni."\n";	
	}
	
}
