# Recursos PHP

Este repositorio contiene algunos ejemplos practicos que pueden utilizarse en proyectos dependiendo de lo que necesiemos.

Contiene:

+ Captcha
+ Leer archivos excel (Libreria PHPExcel)
+ Generar un codigo QR (Libreria QR Code)
+ Un generador de contraseñas

### QR Code y PHPExcel

Hay que descomprimir el .zip dentro de la misma carpeta que es el que contiene la libreria, se comprimio para reducir espacio en el repositorio.

> NOTA: deben clonarlo en la carpeta public

Prof. Andrés D. Romano
