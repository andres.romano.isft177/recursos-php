<?php 
/**
 * 
 * @var $qrcode = new QRcode('your message here', 'H'); 
 * 
 * la 'H' tambien se puede reemplazar por L, M ó Q, son distintos
 * tipos de codificación
 * 
 * @method displayPNG() hay que pasarle un integer que es la cantidad de pixeles
 * 
 * se debe colocar en el src="" del input <img>
 * 
 * <img src="img.php">
 */

require_once "qrcode.class.php";

$url = "http://localhost/qr.php?id=".$_GET['id'];

$qrcode = new QRcode(utf8_encode($url), 'H');
$qrcode->displayPNG(200);

exit();
