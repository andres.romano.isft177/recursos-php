<?php
/**
  * GENERADOR DE CLAVES/CODIGOS
  * 
  * @method generate() recibe un string combinado de caracteres 
  * 	para generar un código aleatorio, tambien se le puede 
  * 	pasar el largo, integer, de la cadena que sera devuelta
  * 
  * @param string mayus|minus|numbers|symbols se utilizan para armar
  * 	la cadena que se pasa al metodo generate()
  * 
  * @param integer|len si no se especifica por default es 8
  * 
  * @version 1.0
  * @author Prof. Andrés D. Romano
  */
class PassGen {

	public $mayus = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	public $minus = 'abcdefghijklmnopqrstuvwxyz';
	public $numbers = '0123456789';
	public $symbols = '!@#%^&*()_,./<>?;:[]{}\|=+';

	public function generate($sting, $len = 8) {

		$limit = strlen($sting);
		for ($i=0; $i < $len; $i++){
			$code .= $sting[rand(0, $limit)];
		}	
			
		return $code;

	}

}

$code = new PassGen();

$pass = $code->minus;
$pass .= $code->numbers;
$pass .= $code->symbols;
$pass .= $code->mayus;

echo $code->generate($pass,16);
